import java.util.*;
import java.text.*;

public class NaiveBayesv3
{
	
	int n = 0;
	static int count = 0;
	PKK arrP[] = new PKK[10];
	
	public NaiveBayesv3()
	{
		setValues();
		printTable();
	}
	
	void inputData(int n)
	{
		Scanner S = new Scanner(System.in);
		
		for(int i=count; i<count+n; i++)
		{
			arrP[i] = new PKK();
			System.out.println("");
			System.out.print("Input Jenis Kelamin : ");
			arrP[i].setData1(S.next());
			System.out.print("Input Pendidikan : ");
			arrP[i].setData2(S.next());
			System.out.print("Input Bidang Pekerjaan : ");
			arrP[i].setData3(S.next());
			System.out.print("Input Kelompok Usia : ");
			arrP[i].setData4(S.next());
			System.out.print("Input Kartu Kredit : ");
			arrP[i].setData5(S.nextBoolean());
		}
		
		count += n;
		
	}
	
	void setValues()
	{
		arrP[0] = new PKK();
			arrP[0].setValues("Wanita", "S2", "Pendidikan", "Tua", true);
		arrP[1] = new PKK();
			arrP[1].setValues("Pria", "S1", "Marketing", "Muda", true);
		arrP[2] = new PKK();
			arrP[2].setValues("Wanita", "SMA", "Wirausaha", "Tua", true);
		arrP[3] = new PKK();
			arrP[3].setValues("Pria", "S1", "Profesional", "Tua", true);
		arrP[4] = new PKK();
			arrP[4].setValues("Pria", "S2", "Profesional", "Muda", false);
		arrP[5] = new PKK();
			arrP[5].setValues("Pria", "SMA", "Wirausaha", "Muda", false);
		arrP[6] = new PKK();
			arrP[6].setValues("Wanita", "SMA", "Marketing", "Muda", false);
		count += 7;
	}
	
	void option()
	{
		Scanner S = new Scanner(System.in);
		
		System.out.println("");
		System.out.println("1. Tambah Data");
		System.out.println("2. Kalkulasikan Data Baru");
		System.out.println("0. Exit");
		System.out.print("Pilih option : ");
		int optionNum = S.nextInt();
		
		switch(optionNum)
		{
			case 1:
				System.out.print("Berapa banyak data yang ingin di iput : ");
				inputData(S.nextInt());
				printTable();
			case 2:
				newValues();
			default:
				System.exit(0);
		}
	}
	
	void newValues()
	{
		String JK = "";
		String Pendidikan = "";
		String Pekerjaan = "";
		String Usia = "";
		boolean KK = false;
		double sumTrue = 0;
		double sumFalse = 0;
		double sum = 0;
		
		Scanner S = new Scanner(System.in);
		System.out.println(" ");
		System.out.print("Input Jenis Kelamin : "); JK = S.next();
		System.out.print("Input Pendidikan : "); Pendidikan = S.next();
		System.out.print("Input Bidang Pekerjaan : "); Pekerjaan = S.next();
		System.out.print("Input Kelompok Usia : "); Usia = S.next();
		
		System.out.println("\n Formula : P(A|B) = P(A) PHI P(B|A) / P(B)\n");
		sumTrue = calcTrue(JK, Pendidikan, Pekerjaan, Usia);
		sumFalse = calcFalse(JK, Pendidikan, Pekerjaan, Usia);
		System.out.println("P(A) = "+sumTrue);
		System.out.println("P(B) = "+sumFalse);
		sum = (sumTrue/(sumTrue+sumFalse))*100;
		System.out.println("P(A|B) = "+(Double.parseDouble(new DecimalFormat("##.##").format(sum)))+"%");
	}
	
	double calcTrue(String JK, String Pendidikan, String Pekerjaan, String Usia)
	{
		double PKKTrue = 0;
		double PKKTrueD1 = 0;
		double PKKTrueD2 = 0;
		double PKKTrueD3 = 0;
		double PKKTrueD4 = 0;
		double trueValue = 0;
		double trueJKValue = 0;
		double truePendidikanValue = 0;
		double truePekerjaanValue = 0;
		double trueUsiaValue = 0;
		
		
		for(int i=0; i<count; i++)
		{
			if(arrP[i].getData5())
			{
				trueValue++;
			}	
		}
		
		PKKTrue = trueValue / count;
		
		for(int i=0; i<count; i++)
		{
			if(JK.equals(arrP[i].getData1()) && arrP[i].getData5())
			{
				++trueJKValue;
			}	
		}
		
		PKKTrueD1 = trueJKValue / trueValue;

		for(int i=0; i<count; i++)
		{
			if(Pendidikan.equals(arrP[i].getData2()) && arrP[i].getData5())
			{
				++truePendidikanValue;
			}
		}
		
		PKKTrueD2 = truePendidikanValue / trueValue;
		
		for(int i=0; i<count; i++)
		{
			if(Pekerjaan.equals(arrP[i].getData3()) && arrP[i].getData5())
			{
				++truePekerjaanValue;
			}
		}
		
		PKKTrueD3 = truePekerjaanValue / trueValue;
		
		for(int i=0; i<count; i++)
		{
			if(Usia.equals(arrP[i].getData4()) && arrP[i].getData5())
			{
				++trueUsiaValue;
			}
		}
		
		PKKTrueD4 = trueUsiaValue / trueValue;
		
		return (PKKTrue * PKKTrueD1 * PKKTrueD2 * PKKTrueD3 * PKKTrueD4);
		
	}

	double calcFalse(String JK, String Pendidikan, String Pekerjaan, String Usia)
	{
		double PKKFalse = 0;
		double PKKFalseD1 = 0;
		double PKKFalseD2 = 0;
		double PKKFalseD3 = 0;
		double PKKFalseD4 = 0;
		double falseValue = 0;
		double falseJKValue = 0;
		double falsePendidikanValue = 0;
		double falsePekerjaanValue = 0;
		double falseUsiaValue = 0;
		
		
		for(int i=0; i<count; i++)
		{
			if(!arrP[i].getData5())
			{
				falseValue++;
			}	
		}
		
		PKKFalse = falseValue / count;
		
		for(int i=0; i<count; i++)
		{
			if(JK.equals(arrP[i].getData1()) && !arrP[i].getData5())
			{
				++falseJKValue;
			}	
		}
		
		PKKFalseD1 = falseJKValue / falseValue;

		for(int i=0; i<count; i++)
		{
			if(Pendidikan.equals(arrP[i].getData2()) && !arrP[i].getData5())
			{
				++falsePendidikanValue;
			}
		}
		
		PKKFalseD2 = falsePendidikanValue / falseValue;
		
		for(int i=0; i<count; i++)
		{
			if(Pekerjaan.equals(arrP[i].getData3()) && !arrP[i].getData5())
			{
				++falsePekerjaanValue;
			}
		}
		
		PKKFalseD3 = falsePekerjaanValue / falseValue;
		
		for(int i=0; i<count; i++)
		{
			if(Usia.equals(arrP[i].getData4()) && !arrP[i].getData5())
			{
				++falseUsiaValue;
			}
		}
		
		PKKFalseD4 = falseUsiaValue / falseValue;
		
		return (PKKFalse * PKKFalseD1 * PKKFalseD2 * PKKFalseD3 * PKKFalseD4);
		
	}	
	
	void printTable()
	{
		System.out.println("\nJenis Kelamin\tPendidikan\tPekerjaan\tKelompok Usia\tKartu Kredit");
		for(int i=0; i<count; i++)
		{
			System.out.println(arrP[i].getData1()+"\t\t"+arrP[i].getData2()+"\t\t"+arrP[i].getData3()+"\t"+arrP[i].getData4()+"\t\t"+arrP[i].getData5());
		}
		
		option();
	}
	
	static void test()
	{	
		NaiveBayesv3 NB = new NaiveBayesv3();
	}
	
	public static void main(String args[])
	{
		test();
	}
}