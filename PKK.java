public class PKK
{
	public String data1 = "";
	public String data2 = "";
	public String data3 = "";
	public String data4 = "";
	public Boolean data5 = true;
	
	public PKK()
	{
		this.data1 = data1;
		this.data2 = data2;
		this.data3 = data3;
		this.data4 = data4;
		this.data5 = data5;
	}
	
	public void setData1(String s)
	{
		this.data1 = s;
	}
	
	public void setData2(String s)
	{
		this.data2 = s;
	}
	
	public void setData3(String s)
	{
		this.data3 = s;
	}
	
	public void setData4(String s)
	{
		this.data4 = s;
	}
	
	public void setData5(boolean b)
	{
		this.data5 = b;
	}
	
	public void setValues(String s, String t, String u, String v, boolean b)
	{
		this.data1 = s;
		this.data2 = t;
		this.data3 = u;
		this.data4 = v;
		this.data5 = b;
	}
	
	public String getData1()
	{
		return data1;
	}
	
	public String getData2()
	{
		return data2;
	}
	
	public String getData3()
	{
		return data3;
	}
	
	public String getData4()
	{
		return data4;
	}
	
	public boolean getData5()
	{
		return data5;
	}
}